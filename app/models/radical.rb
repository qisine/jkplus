class Radical < ActiveRecord::Base
  belongs_to :character
  has_many :radical_variants
end
