class Word < ActiveRecord::Base
  has_many :word_details

  def self.ac_search(search_string)
    return [] unless (search_string = sanitize_search_string(search_string))

    if search_string[Regexp.new("[#{HanziMethods::CHINESE_CHARACTERS}]")]
      ac_hanzi_search(search_string)
    else
      ac_details_search(search_string)
    end
  end

  def self.ac_hanzi_search(search_string)
    Word
      .where("hanyu_wo_punctuation like '#{search_string}%'")
      .order("frequency nulls last")
      .limit(10)    
      .to_a
      .map(&:hanyu)
  end

  def self.ac_details_search(search_string)
    sql = "pinyin_wo_spaces#{'_and_nos' if !(search_string =~ /\d/)} ILIKE '#{search_string}%'" 

    Word
      .joins(:word_details)
      .where(sql)
      .order('frequency asc nulls last')
      .uniq
      .select([:hanyu, :frequency])
      .limit(10)
      .to_a
      .map(&:hanyu)
  end
end
