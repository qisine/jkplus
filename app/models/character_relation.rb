class CharacterRelation < ActiveRecord::Base
  belongs_to :character
  belongs_to :related_character, class_name: "Character"
end
