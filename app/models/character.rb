class Character < ActiveRecord::Base
  has_many :character_relations
  has_many :related_characters, foreign_key: :related_character_id, class_name: "CharacterRelation"
  has_many :parents, through: :related_characters, source: :character
  has_many :children, through: :character_relations, source: :related_character

  has_one :radical
  has_many :radical_variants, through: :radicals
end
