#encoding: utf-8

class Sentence < ActiveRecord::Base
  def self.search(search_string)
    return where("1=1") if search_string.blank?

    search_string = sanitize_search_string(search_string, allow_wildcards: true)

    props = case search_string
    when Regexp.new("\\A[#{HanziMethods::CHINESE_CHARACTERS}]+\\z")
      { column: "hanyu", ldel: "", rdel: "" }
    else
      { column: "english", ldel: "\\m", rdel: "\\M" }
    end

    where("#{props[:column]} similar to '%#{props[:ldel]}#{search_string}#{props[:rdel]}%'")
  end

  def to_a;[english, hanyu];end
  def to_csv;to_a.to_csv;end
  def to_text;"#{english}\t#{hanyu}";end
end
