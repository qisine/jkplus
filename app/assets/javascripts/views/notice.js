App.Views.Notice = Backbone.View.extend({
  view_type: "Notice",
  events: {
    "click .dismissable": "dismiss"
  },

  initialize: function() {
    this.message = this.options.message;
  },

  render: function() {
    var templ = _.template($("#" + this.view_type + "_template").html());
    $(this.el).html(templ({ message: this.message }));
  },

  dismiss: function() {
    this.remove();
    this.off();
  },

  onClose: function() {
  
  }
});
