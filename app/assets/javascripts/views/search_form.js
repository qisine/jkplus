App.Views.SearchForm = Backbone.View.extend({
  view_type: "SearchForm",
  events: {
    "click input[type=submit]": "search",
  },

  initialize: function() {
    this.search_string = this.options.search_string;
    _.bindAll(this, "update_search_string");
    App.dispatcher.on("sentences:search:success", this.update_search_string);
  },

  search: function(ev) {
    ev.preventDefault();
    search_string = $("#search_string").val();

    new App.Views.SentencesTable({ search_string: search_string });
  },

  update_search_string: function(search_string) {
    var cur = $("#search_string");
    if(cur !== search_string) { $("#search_string").val(search_string); }
  },

  render: function() {
    var templ = _.template($("#" + this.view_type + "_template").html());
    this.$el.html(templ());
    this.$el.find("#search_string").autocomplete({
      source: 'words/ac',
    });
  },

  onClose: function() {
    this.undelegateEvents();
    App.dispatcher.off("sentences:search:success", this.update_search_string);
  },
});
