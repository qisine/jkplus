App.Views.Paginatable =
  events:
    "click a#p_next": "next"
    "click a#p_previous": "previous"
    "change select#p_selection": "go_to"

  initialize: ->
    _.bindAll(this, "next", "previous", "render")
    @collection = (@options.collection || new App.Collections.Sentences)
    page = parseInt(this.options.page, 10) || 1
    page = 1 if page < 1
    @collection.page = page
    @collection._pag_params = 
      search_string: $.trim(@options.search_string)

  render: ->
    templ = _.template($("#Paginatable_template").html())
    return templ({ info: @collection.page_info() })
    
  go_to: (ev) ->
    ev.preventDefault();
    @collection.go_to(parseInt($(ev.target).attr("value"), 10) || -1)

  next: (ev) ->
    @collection.next_page()
    return false

  previous: (ev) ->
    @collection.previous_page()
    return false

  onClose: ->
    @undelegateEvents()
    @collection.off
    @off()
    @remove()
