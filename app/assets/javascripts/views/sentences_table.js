App.Views.SentencesTable = Backbone.View.extend({
  view_type: "SentencesTable",
  events: {
    "click .hiding" : "toggle_english",
  },

  initialize: function() {
    this.call_pag_method().initialize();
    this.collection = (this.collection || new App.Collections.Sentences);
    this.search_string = $.trim(this.options.search_string);
    _.bindAll(this, "insertIntoDOM", "toggle_english_all");
    this.collection.on("reset", this.insertIntoDOM);
    App.dispatcher.on("sentences:settings:toggle_english_all", this.toggle_english_all);
    this.repopulate();
  },

  repopulate: function() {
    var that = this;
    if(this.search_string) {
      this.collection._insert_view = true;
      this.collection.fetch({
          data: { search_string: that.search_string },
          success: function() {
                    App.dispatcher.trigger(
                      "sentences:search:success", that.search_string, that.collection.page);
                   },
          error: function() {
                    App.Views.render_and_insert(
                      new App.Views.Notice({ message: "Error during search!" }));
                 }
          });
    } else {
      App.Views.render_and_insert(this);
    }
  },

  insertIntoDOM: function() {
    App.dispatcher.trigger("sentences:search:success", this.search_string, this.collection.page);
    this.render();
    if(this.collection._insert_view) {
      App.Views.render_and_insert(this);
    }
  },

  toggle_english_all : function(value) {
    this.render();
  },

  toggle_english : function(ev) {
    $(ev.target)
      .text($(ev.target).text() === "+" ? "-" : "+") 
      .closest('.row')
      .siblings()
      .toggle();
    return false;
  },

  clear: function() {
    this.$el.html("");
  },

  render: function() {
    if(!this.collection.isEmpty()) {
      var templ = _.template($("#" + this.view_type + "_template").html());
      this.$el.html(templ({ collection: this.collection, show_english: App.settings.get("show_english") }));
      this.$el.append(this.call_pag_method().render());
    } else {
      this.clear();
    }
  },
  
  onClose: function() {
    this.call_pag_method().onClose();
    this.undelegateEvents();
  },
});

App.Views.SentencesTable.mixin(App.Views.Paginatable);
