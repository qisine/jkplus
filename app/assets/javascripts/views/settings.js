App.Views.Settings = Backbone.View.extend({
  view_type : "Settings",
  events : {
    "change #show_english" : "toggle_english_all",
  },

  initialize: function() {
    this.model = this.options.model; 
  },

  toggle_english_all: function() {
    this.model.show_english = !this.model.show_english;
    App.dispatcher.trigger("sentences:settings:toggle_english_all", this.model.show_english);
  },

  render: function() {
    var templ = _.template($("#" + this.view_type + "_template").html());
    this.$el.html(templ({ model: this.model}));
  },
});
