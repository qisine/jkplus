App.Views.Main = Backbone.View.extend({
  view_type: "Main",
  events: {

  },

  initialize: function() {
  },

  render: function() {
    var templ = _.template($("#" + this.view_type + "_template").html());
    $(this.el).html(templ());
  },
});
