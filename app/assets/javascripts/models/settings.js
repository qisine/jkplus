App.Models.Settings = Backbone.Model.extend({
  defaults: {
    show_english: false,
    disable_autocomplete: false,
  },

  url : function() {
    var base = 'settings';
    if(this.isNew()) return base;
    return base + '/' + this.id;
  },
});

App.Models.SettingsWrapper = function(model) {
  this.model = model;
}

App.Models.SettingsWrapper.prototype = {
  get : function(name) {
    return this.model[name];
  }
}

