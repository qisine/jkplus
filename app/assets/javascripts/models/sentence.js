var Sentence = Backbone.Model.extend({
  url : function() {
    var base = 'sentences';
    if(this.isNew()) return base;
    return base + '/' + this.id;
  }
});
