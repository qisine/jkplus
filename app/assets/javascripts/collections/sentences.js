App.Collections.Sentences = Backbone.Collection.extend({
  model: Sentence,
  url: '/sentences/search',

  initialize: function() {
    this.call_pag_method().initialize();
  },

  go_to: function(page) {
    this._insert_view = false;
    this.call_pag_method().go_to(page);
  },
});

App.Collections.Sentences.mixin(App.Collections.Paginatable);
