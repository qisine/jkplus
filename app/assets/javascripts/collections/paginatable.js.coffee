App.Collections.Paginatable =
  initialize: ->
    _.bindAll(this, "parse", "fetch", "page_info", "next_page", "previous_page")
    @page = 1
    @per_page = (@per_page || 25)

  fetch: (options) ->
    @trigger("fetching")

    options = (options || {})
    options.data = (options.data || {})
    _.defaults(options.data, @_pag_params, { page: @page, per_page: @per_page }) 

    that = this
    if options.success
      success = options.success
      options.success = (response) ->
        that.trigger("fetched")
        success(response)
    return Backbone.Collection.prototype.fetch.call(this, options) 

  parse: (response, xhr) ->
    @page = parseInt(response.page, 10) 
    @total = response.total
    @per_page = response.per_page
    return response.models 

  page_info: ->
    info =
      total: @total
      page: @page
      per_page: @per_page
      pages: Math.ceil(@total / @per_page)
      prev: 0
      next: 0

    info.prev = @page - 1 if @page > 1
    info.next = @page + 1 if @page < info.pages

    return info

  go_to: (page) ->
    return if page > @page_info().pages + 1 || page < 1
    @page = page

    return this.fetch()

  next_page: ->
    #return if !@page_info().next
    @go_to(@page + 1)

  previous_page: ->
    #return if !@page_info().prev
    @go_to(@page - 1)
