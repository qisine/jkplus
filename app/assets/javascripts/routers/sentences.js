App.Routers.Sentences = Backbone.Router.extend({
  routes: {
    "": "index",
    "search": "index",
    "search/:search_string":  "search",
    "search/:search_string/p/:page" : "search",
  },

  initialize: function() {
    _.bindAll(this, "navigate_to");
    App.dispatcher.bind("sentences:search:success", this.navigate_to);
  },

  index: function() {
    var settings = new App.Views.Settings({ model: this._instantiate_settings() });
    App.Views.render_and_insert(new App.Views.Main);
    App.Views.render_and_insert(new App.Views.SearchForm);
    App.Views.render_and_insert(settings);
    new App.Views.SentencesTable;
  },

  search: function(search_string, page) {
    var settings = new App.Views.Settings({ model: this._instantiate_settings() });
    App.Views.render_and_insert(new App.Views.Main);
    App.Views.render_and_insert(new App.Views.SearchForm);
    App.Views.render_and_insert(settings);
    search_string = $.trim(decodeURIComponent(search_string));
    if(search_string) {
      new App.Views.SentencesTable({ search_string: search_string, page: page });
    }
  },

  _instantiate_settings : function() {
    if(!App.settings) {
      var model = new App.Models.Settings;
      App.settings = new App.Models.SettingsWrapper(model);
    }
    return model;
  },

  navigate_to: function(search_string, page) {
        this.navigate("search/" + search_string + "/p/" + page); 
  },
});
