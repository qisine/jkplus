(function($) {
  $.fn.jk_tooltip = function(options) {
    var settings = $.extend({
      text: "welcome tooltip",
      data_text_id: "jk-tooltip-text",
      container: "div",
      css_class: "jk_tooltip",
      default_dimensions: { w: 100, h: 100 },
      default_offset: { x: 30, y: 30 },
      default_distance: 5,
    }, options);
    
    function calcPosAndDim(tt, side) {
      var sides = side === "Left" ?
                             { here: "left", there: "right", dim: "w" } :
                             { here: "top", there: "bottom", dim: "h"};
      var dd = side === "Left" ? settings.default_distance : 0;
      var st = side === "Left" ? help_icon[sides.there] : help_icon[sides.here];
       
      tt.pos = tt.pos || {}

      var cutoffs = {
        here: (tt.dim[sides.dim] - help_icon[sides.here] - dd),
        there: (st + dd + tt.dim[sides.dim]) - window.innerWidth,
      }

      if(cutoffs.here <= 0 || cutoffs.there <= 0) {
        tt.pos[sides.here] = cutoffs.there <= 0 ?
                              st + dd :
                              help_icon[sides.here] - dd;
      } else {
        tt.pos[sides.here] = cutoffs.there < cutoffs.here ?
                              st + dd - cutoffs.there :
                              (help_icon[sides.here] - dd) + cutoffs.here;
      }

      tt.pos[sides.here] = tt.pos[sides.here] + $(window)["scroll" + side]();
      
      return tt;
    }

    function closeTooltip(ev) {
      $(ev.target).parent().hide();
      return false; 
    }

    var that = this;
    var help_icon = this.get(0).getBoundingClientRect();
    var tt = {
      dim: {
        w: Math.min($(window).innerWidth() - settings.default_distance, settings.default_dimensions.w),
        h: Math.min($(window).innerHeight() - settings.default_distance, settings.default_dimensions.h),
      },
    }

    calcPosAndDim(calcPosAndDim(tt, "Left"), "Top");

    var tt_el =  "<div class='" + _.escape(settings.css_class) + "'>" +
                   "<a href='#'>X</a>" +
                   _.escape(settings.text) +
                 "</div>";

    this.on("click", function() { $(tt_el).show(); return false; });
    tt_el = $(tt_el).width(tt.dim.w).height(tt.dim.h).offset(tt.pos).insertAfter(this).css("position", "absolute").hide();
    tt_el.find("a").on("click", closeTooltip); 
    
    return this;
  }
})(jQuery);
