App.mixin = (from) ->
  to = this.prototype
  to._pag_meths = {}
  to.call_pag_method = ->
    this._pag_meths.that = this
    return this._pag_meths

  _.defaults(to, from)

  _.defaults(to.events, from.events) if to instanceof Backbone.View

  if to instanceof Backbone.Collection
    to["parse"] = from["parse"]
    to["fetch"] = from["fetch"]

  methods = ["initialize"]

  methods.push("render", "onClose") if to instanceof Backbone.View
  methods.push("go_to") if to instanceof Backbone.Collection

  _.each methods, (method) ->
    if to[method]
      f = from
      to._pag_meths[method] = ->
          f[method].apply(this.that, arguments)
 
b = Backbone
b.Model.mixin = b.Collection.mixin = b.View.mixin = App.mixin
