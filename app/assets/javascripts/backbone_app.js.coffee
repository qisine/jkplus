@App =
  Views: {}
  Routers: {}
  Collections: {}
  Models: {}
  init: ->
    new App.Routers.Sentences
    Backbone.history.start()
  dispatcher: _.extend({}, Backbone.Events)
  
@App.Views.render_and_insert = (new_view) ->
  function_this = (-> return this)()

  old_view = function_this[new_view.view_type]
  if old_view
    old_view.close() 
    new_view.model = old_view.model if new_view instanceof App.Views.Settings


  new_view.render()
  $("#" + new_view.view_type + "_placeholder").after($(new_view.el))
   
  function_this[new_view.view_type] = new_view

  return new_view

#change interpolation characters, so as not to conflict with ERB
_.templateSettings =
      evaluate: /\{\{([\s\S]+?)\}\}/g,
      interpolate: /\{\{=([\s\S]+?)\}\}/g,

Backbone.View.prototype.close = ->
  @remove()
  @off()

  @onClose() if @onClose
