class SentencesController < ApplicationController
  def index
    @sentences = Sentence.all

    respond_to do |r|
      r.text { render text: @sentences.map(&:to_text).join("\n") }
      r.json { render json: @sentences.to_json }
      r.csv { render text: @sentences.map(&:to_csv).to_csv }
      r.html {
        #@sentences = []
      }
    end
  end

  def search
    @sentences = Sentence.search(params[:search_string]).order('id asc')
    size = @sentences.size
    @sentences = @sentences.page(params[:page]).per(params[:per_page])

    respond_to do |r|
      r.text { render text: @sentences.map(&:to_text).join("\n") }
      r.json {
        render json: {  models: @sentences,
                        per_page: params[:per_page] || Kaminari.config.default_per_page,
                        total: size,
                        page: params[:page] }.to_json
              }
      r.csv { render text: @sentences.map(&:to_csv).to_csv }
      r.html #{ #@sentences = @sentences.page(params[:page]) }
    end 
  end
end
