class WordsController < ApplicationController
  def ac
    @words = Word.ac_search(params[:term])
    respond_to do |r|
      r.json { render json: @words.to_json }
    end
  end
end
