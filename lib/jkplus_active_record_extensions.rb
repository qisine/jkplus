#encoding: utf-8

module JkplusActiveRecordExtensions
  extend ActiveSupport::Concern

  module ClassMethods
    def sanitize_search_string(search_string, options={})
      allow_wildcards = options.delete(:allow_wildcards)

      punct_rgx = Regexp.new("\\A[#{HanziMethods::ALL_PUNCTUATION_AND_SPACES}]+\z")
      return nil if search_string.blank? || search_string =~ punct_rgx

      char_rgx = HanziMethods::CHINESE_CHARACTERS + HanziMethods::ENGLISH_CHARACTERS_AND_NUMBERS 
      char_rgx += "_*" if allow_wildcards

      search_string = sanitize(search_string.scan(Regexp.new("[#{char_rgx}]+")).join)
      search_string.gsub("'", "").gsub("*", "%").strip
    end
  end
end

ActiveRecord::Base.send(:include, JkplusActiveRecordExtensions)
