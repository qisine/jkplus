#encoding: utf-8

require 'spec_helper'

describe Word do
  describe ".ac_search" do
    it "returns empty array when argument is blank" do
      ["", nil].each do |e|
        Word.ac_search(e).should be_empty
      end
    end

    it "returns max 10 elements" do
      [nil, "", "p", "shi1", "是", "添加", "在*"].each do |e|
        Word.ac_search(e).should have_at_most 10
      end
      Word.ac_search("sh").should_not include "丗"
    end

    it "disregards spaces, case, or punctuation" do
      ["佛，，山！", ", __thiS, .", "三：", " happy   NEW", "grEAt*"].each do |e|
        Word.ac_search(e).should_not be_empty
      end
    end

    it "accepts pinyin both with and without tones" do
      ["shi1", "shi"].each do |e|
        Word.ac_search(e).should include "是" 
      end
      ["ni3", "ni"].each do |e|
        Word.ac_search(e).should include "你" 
      end
    end

    it "only returns hanzis, without punctuation" do
      [nil, "a!*", "s,", "学！", "《天生》"].each do |e|
        Word.ac_search(e).each do |r|
          r.should_not match Regexp.new("[#{HanziMethods::ALL_PUNCTUATION_AND_SPACES}]")
        end
      end
    end

    it "orders results by word frequency, most frequent first" do
      { sh: "是", zai: "在", bu4: "不", hao: "好" }.each_pair do |k,v|
        Word.ac_search(k.to_s).first.should eq v
      end
    end
  end

  describe ".ac_hanzi_search" do
  end

  describe ".ac_details_search" do
  end
end
