#encoding: utf-8

require 'spec_helper'

describe JkplusActiveRecordExtensions do
  before :all do
    let :klass do
      Class.new do
        extend JkplusActiveRecordExtensions::ClassMethods
      end
    end
  end

  describe ".sanitize_search_string" do
    it "returns nil on blank input" do
        klass.sanitize_search_string("").should be_nil
        klass.sanitize_search_string(nil, allow_wildcards: true).should be_nil
    end

    it "removes spaces" do
      klass.sanitize_search_string("   \n  ").should be_nil
      klass.sanitize_search_string("ab  c \r   d", allow_wildcards: true).should eq("abcd")
      klass.sanitize_search_string("学\n\t88ab    ").should eq("学88ab")
    end

    context "when allow_wildcards false" do
      it "removes punctuation" do
        klass.sanitize_search_string(",;！《  **_ ").should be_empty
        klass.sanitize_search_string("a,;88!<>,''\"\":**_$@**)(\\").should eq("a88$@")
        klass.sanitize_search_string("到底，达到，抵达！！").should eq("到底达到抵达")
      end
    end

    context "when allow_wildcards true" do
      it "replaces '*' with '%'" do
        klass.sanitize_search_string("*", allow_wildcards: true).should eq("%")
      end

      it "accepts both '*' and '_' as wildcard characters" do
        klass.sanitize_search_string("abc_ef*", allow_wildcards: true).should eq("abc_ef%")
        klass.sanitize_search_string(" a __ 0-9 * xyz", allow_wildcards: true).should eq("a0-9%xyz")
      end

      it "removes non-allowed punctuation" do
        klass.sanitize_search_string("  :'  \r\n ！《》", allow_wildcards: true).should be_empty
        klass.sanitize_search_string(",;！《  **_ ", allow_wildcards: true).should eq("%%_")
        klass.sanitize_search_string("a,;8!<>,'\":*_$@**)(\\", allow_wildcards: true).should eq("a8%_$@%%")
        klass.sanitize_search_string("到底，达到，抵达！！", allow_wildcards: true).should eq("到底达到抵达")
      end
    end
  end
end
  
