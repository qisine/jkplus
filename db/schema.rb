# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120801082403) do

  create_table "character_relations", :force => true do |t|
    t.integer "character_id"
    t.integer "related_character_id"
    t.string  "composition_type"
  end

  add_index "character_relations", ["character_id"], :name => "index_character_relations_on_character_id"
  add_index "character_relations", ["related_character_id"], :name => "index_character_relations_on_related_character_id"

  create_table "characters", :force => true do |t|
    t.string "hanzi"
  end

  add_index "characters", ["hanzi"], :name => "index_characters_on_hanzi"

  create_table "radical_variants", :force => true do |t|
    t.integer "radical_id"
    t.string  "hanzi"
  end

  add_index "radical_variants", ["radical_id"], :name => "index_radical_variants_on_radical_id"

  create_table "radicals", :force => true do |t|
    t.integer "character_id"
    t.string  "name"
  end

  add_index "radicals", ["character_id"], :name => "index_radicals_on_character_id"

  create_table "s1_compositions", :id => false, :force => true do |t|
    t.string "child",            :limit => 10
    t.string "parent",           :limit => 10
    t.string "composition_type", :limit => 10
  end

  add_index "s1_compositions", ["child"], :name => "cidx"
  add_index "s1_compositions", ["parent"], :name => "pidx"

  create_table "s1_frequency", :id => false, :force => true do |t|
    t.string  "c"
    t.integer "max"
  end

  create_table "s1_mdbg", :id => false, :force => true do |t|
    t.string "h"
    t.string "p"
    t.string "e", :limit => 1024
  end

  create_table "s1_radicals", :id => false, :force => true do |t|
    t.string "name"
    t.string "character", :limit => 10
  end

  create_table "s1_rv", :id => false, :force => true do |t|
    t.string "radical",   :limit => 10
    t.string "character", :limit => 10
  end

  create_table "s1_sentences", :id => false, :force => true do |t|
    t.text "e"
    t.text "h"
  end

  create_table "sentences", :force => true do |t|
    t.text     "hanyu"
    t.text     "english"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sentences", ["english"], :name => "index_sentences_on_english"
  add_index "sentences", ["hanyu"], :name => "index_sentences_on_hanyu"

  create_table "word_details", :force => true do |t|
    t.integer  "word_id"
    t.string   "pinyin"
    t.text     "english"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "pinyin_wo_spaces"
    t.string   "pinyin_wo_spaces_and_nos"
  end

  add_index "word_details", ["english"], :name => "index_word_details_on_english"
  add_index "word_details", ["pinyin"], :name => "index_word_details_on_pinyin"
  add_index "word_details", ["pinyin_wo_spaces"], :name => "index_word_details_on_pinyin_wo_spaces"
  add_index "word_details", ["pinyin_wo_spaces_and_nos"], :name => "index_word_details_on_pinyin_wo_spaces_and_nos"
  add_index "word_details", ["word_id"], :name => "index_word_details_on_word_id"

  create_table "words", :force => true do |t|
    t.string   "hanyu"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.integer  "frequency"
    t.string   "hanyu_wo_punctuation"
  end

  add_index "words", ["hanyu"], :name => "index_words_on_hanyu"
  add_index "words", ["hanyu_wo_punctuation"], :name => "index_words_on_hanyu_wo_punctuation"

end
