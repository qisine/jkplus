class CreateCharacterRelations < ActiveRecord::Migration
  def change
    create_table :character_relations do |t|
      t.integer :character_id
      t.integer :related_character_id
      t.string :composition_type
    end
  end
end
