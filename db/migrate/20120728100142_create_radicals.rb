class CreateRadicals < ActiveRecord::Migration
  def change
    create_table :radicals do |t| 
      t.integer :character_id
      t.integer :name 
    end
  end
end
