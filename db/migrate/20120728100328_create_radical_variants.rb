class CreateRadicalVariants < ActiveRecord::Migration
  def change
    create_table :radical_variants do |t|
      t.integer :radical_id
      t.string :hanzi
    end
  end
end
