class CreateSentences < ActiveRecord::Migration
  def change
    create_table :sentences do |t|
      t.string :hanyu
      t.string :english

      t.timestamps
    end
  end
end
