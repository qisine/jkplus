class DropWordsHanyuEnglish < ActiveRecord::Migration
  def up
    remove_column :words, :pinyin
    remove_column :words, :english
  end

  def down
    add_column :words, :pinyin, :string
    add_column :words, :english, :text
  end
end
