class CreateWordDetails < ActiveRecord::Migration
  def change
    create_table :word_details do |t|
      t.references :word
      t.string :pinyin
      t.text :english

      t.timestamps
    end
    add_index :word_details, :word_id
    add_index :word_details, :pinyin
    add_index :word_details, :english
  end
end
