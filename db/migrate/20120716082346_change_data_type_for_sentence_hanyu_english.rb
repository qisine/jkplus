class ChangeDataTypeForSentenceHanyuEnglish < ActiveRecord::Migration
  def up
    change_column :sentences, :hanyu, :text
    change_column :sentences, :english, :text
  end

  def down
    change_column :sentences, :hanyu, :string
    change_column :sentences, :english, :string
  end
end
