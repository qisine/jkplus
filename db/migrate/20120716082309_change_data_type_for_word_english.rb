class ChangeDataTypeForWordEnglish < ActiveRecord::Migration
  def up
    change_column :words, :english, :text
  end

  def down
    change_column :words, :english, :string
  end
end
