class AddIndexes < ActiveRecord::Migration
  def change
    add_index :characters, :hanzi
    add_index :character_relations, :character_id
    add_index :character_relations, :related_character_id
    add_index :radicals, :character_id
    add_index :radical_variants, :radical_id
    add_index :sentences, :hanyu
    add_index :sentences, :english
    add_index :words, :hanyu
  end
end
