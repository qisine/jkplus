class ChangeRadicalNameType < ActiveRecord::Migration
  def change
    change_column :radicals, :name, :string
  end
end
