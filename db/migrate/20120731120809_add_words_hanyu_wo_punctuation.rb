class AddWordsHanyuWoPunctuation < ActiveRecord::Migration
  def change
    add_column :words, :hanyu_wo_punctuation, :string
    add_index :words, :hanyu_wo_punctuation
  end
end
