class AddWordsPinyinColumns < ActiveRecord::Migration
  def change
    add_column :word_details, :pinyin_wo_spaces, :string
    add_column :word_details, :pinyin_wo_nos, :string
    add_column :word_details, :pinyin_wo_spaces_and_nos, :string

    add_index :word_details, :pinyin_wo_spaces
    add_index :word_details, :pinyin_wo_nos
    add_index :word_details, :pinyin_wo_spaces_and_nos
  end
end
