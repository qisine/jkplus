Jkplus::Application.routes.draw do
  resources :sentences, only: [:index] do
    get :search, on: :collection
  end

  match 'sentences/search/:search_string' => 'sentences#search', as: :restful_sentences_search
  match 'words/ac' => 'words#ac'

  root to: 'sentences#index'
end
