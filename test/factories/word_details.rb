# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :word_detail do
    word_id nil
    pinyin "MyString"
    english "MyText"
  end
end
