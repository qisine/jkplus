# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sentence do
    hanyu "MyString"
    english "MyString"
  end
end
